package com.rangsiman.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class RobotTest {
    
    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(10, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", 'R');
        assertEquals("Robot", robot.getName());
        assertEquals('R', robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldCreateUpSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up();
        assertEquals(true, result);
        assertEquals(10, robot.getY()); 
    }

    @Test
    public void shouldCreateUpAtMin() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MIN);
        boolean result = robot.up();
        assertEquals(false, result);
        assertEquals(Robot.Y_MIN, robot.getY()); 
    }

    @Test
    public void shouldCreateUpNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(5);
        assertEquals(true, result);
        assertEquals(6, robot.getY()); 
    }

    @Test
    public void shouldCreateUpNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY()); 
    }

    @Test
    public void shouldCreateUpNFail1() {
        Robot robot = new Robot("Robot", 'R', 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY()); 
    }

    @Test
    public void shouldCreateDownSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY()); 
    }

    @Test
    public void shouldCreateDownNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(5);
        assertEquals(true, result);
        assertEquals(14, robot.getY()); 
    }

    @Test
    public void shouldCreateDownNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(10, robot.getY()); 
    }

    @Test
    public void shouldCreateDownNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 9);
        boolean result = robot.down(11);
        assertEquals(false, result);
        assertEquals(19, robot.getY()); 
    }


    @Test
    public void shouldCreateDownBeforeMaxSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX-1);
        boolean result = robot.down();
        assertEquals(true, result);
        assertEquals(Robot.Y_MAX, robot.getY()); 
    }

    @Test
    public void shouldCreateDownAtMax() {
        Robot robot = new Robot("Robot", 'R', 10, Robot.Y_MAX);
        boolean result = robot.down();
        assertEquals(false, result);
        assertEquals(Robot.Y_MAX, robot.getY()); 
    }

    @Test
    public void shouldCreateLeftSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left();
        assertEquals(true, result);
        assertEquals(9, robot.getX()); 
    }

    @Test
    public void shouldCreateLeftNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(2);
        assertEquals(true, result);
        assertEquals(8, robot.getX()); 
    }

    @Test
    public void shouldCreateLeftNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(6);
        assertEquals(true, result);
        assertEquals(4, robot.getX()); 
    }

    @Test
    public void shouldCreateLeftNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.left(11);
        assertEquals(false, result);
        assertEquals(0, robot.getX()); 
    }

    @Test
    public void shouldCreateRightSuccess() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right();
        assertEquals(true, result);
        assertEquals(11, robot.getX()); 
    }

    @Test
    public void shouldCreateRightNSuccess1() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(2);
        assertEquals(true, result);
        assertEquals(12, robot.getX()); 
    }

    @Test
    public void shouldCreateRightNSuccess2() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(6);
        assertEquals(true, result);
        assertEquals(16, robot.getX()); 
    }

    @Test
    public void shouldCreateRightNFail() {
        Robot robot = new Robot("Robot", 'R', 10, 10);
        boolean result = robot.right(11);
        assertEquals(false, result);
        assertEquals(19, robot.getX()); 
    }
}
