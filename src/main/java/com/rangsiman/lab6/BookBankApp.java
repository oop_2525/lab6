package com.rangsiman.lab6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank rangsiman = new BookBank("Rangsiman", 100);
        rangsiman.print();
        rangsiman.deposit(50);
        rangsiman.print();
        rangsiman.withdraw(50);
        rangsiman.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(1000000);
        praweet.withdraw(10000000);
        praweet.print();

        BookBank prayoot = new BookBank("Prayoot", 1);
        prayoot.deposit(10000000);
        prayoot.withdraw(1000000);
        prayoot.print();
    }
}
